//
// core dom utils
//
function createElement(type, props, ...children) {
    const dom = document.createElement(type)
    for (let k in props) {
      if (isEvent(k))
        dom.addEventListener(eventName(k), props[k])
      else
        dom[k] = props[k]
    }
    for (let child of children) {
      if (!(child instanceof Node))
        child = document.createTextNode(child)
      dom.appendChild(child)
    }
    return dom
  }
  function isEvent(key) {
    return key.startsWith("on")
  }
  function eventName(key) {
    return key.toLowerCase().substring(2)
  }
  function render(element, container=o) {
    container.innerHTML = ""
    container.appendChild(element)
  }
  const e = createElement
  //
  // text selection utils
  //
  function findOffsetInParent(node, pos, top) {
    if (node == top)
      return pos;
    while (node.previousSibling) {
      node = node.previousSibling;
      var len = node.nodeType == 3 ? node.length : node.innerText.length;
      pos = len + pos;
    }
    return findOffsetInParent(node.parentNode, pos, top);
  }
  function findChildForOffset(node, pos) {
    var len = node.nodeType == 3 ? node.length : node.innerText.length;
    if (pos > len)
      return findChildForOffset(node.nextSibling, pos - len);
    if (node.firstChild)
      return findChildForOffset(node.firstChild, pos);
    return {node: node, pos: pos};
  }
  //
  // text editor
  //
  let text
  let p = document.createElement("pre")
  let d = document.createElement("pre")
  let row = document.createElement("div")
  d.style["border"] = "1px solid black"
  row.style["display"] = "flex"
  row.appendChild(p)
  row.appendChild(d)
  o.appendChild(row)
  p.setAttribute("contenteditable", false)
  p.setAttribute("spellcheck", false)
  p.style["-webkit-user-modify"] = "read-write-plaintext-only"
  p.style["border"] = "1px solid black"
  p.style["width"] = "250px"
  p.style["height"] = "200px"
  p.style["outline"] = "none"
  p.style["overflow"] = "scroll"
  d.style["width"] = "250px"
  d.style["height"] = "200px"
  d.style["margin-left"] = "10px"
  d.style["overflow"] = "scroll"
  p.addEventListener("beforeinput", ev => {
    ev.preventDefault()
    let data = ev.data || ev.dataTransfer?.getData("text/plain") || ""
    if (ev.inputType == "insertLineBreak")
      data = "\n"
    let range = ev.getTargetRanges()[0]
    // CHROME WORKAROUND!
    let startOffset = 0
    if (!range) {
      range = window.getSelection().getRangeAt(0)
      if (ev.inputType=="deleteContentBackward" && range.startContainer == range.endContainer && range.startOffset == range.endOffset)
        startOffset = 1
    }
    let start = findOffsetInParent(range.startContainer, range.startOffset, p)
    let end = findOffsetInParent(range.endContainer, range.endOffset, p)
    if (start > 0) start -= startOffset
    edit(start, end, data)
    select(start + data.length, start + data.length)
  })
  function edit(start, end, data) {
    text = text.substring(0, start) + data + text.substring(end, text.length)
    p.innerText = ""
    d.innerText = ""
    try {
      let txt = parse(text)
      let ast = walk(txt)
      p.appendChild(txt)
      d.appendChild(ast)
    } catch(ex) {
      print(ex)
    }
  }
  function select(start, end) {
    let sel = window.getSelection()
    let base = findChildForOffset(p, start)
    let extent = findChildForOffset(p, end)
    sel.setBaseAndExtent(base.node, base.pos, extent.node, extent.pos)
  }
  //
  // parser state & lexer
  //
  let input, pos, peek
  let gap
  let str
  let indent
  let seq
  function init(s) {
    seq = []
    input = s;
    pos = 0;
    indent = gap = whitespace();
    read()
  }
  function next() {
    if (gap > 0) seq.push("".padStart(gap, ' ')) // FIXME: comments!
    if (peek != str && peek != '\n')
      seq.push(dom(peek, "", str))
    else
      seq.push(str)
    let c = peek;
    gap = whitespace();
    read()
    return c
  }
  function read() {
    let isdigit = () => '0' <= input[pos] && input[pos] <= '9'
    let isletter = () => 'a' <= input[pos] && input[pos] <= 'z' || 'A' <= input[pos] && input[pos] <= 'Z'
    let start = pos
    if (isdigit()) {
      while (isdigit()) pos++
      peek = "num"
    } else if (isletter()) {
      while (isletter() || isdigit()) pos++
      peek = "ident"
    } else if (input[pos] == '\n') {
      while (input[pos] == '\n') {
        peek = input[pos++]
        indent = whitespace()
      }
    } else {
      peek = input[pos++]
    }
    str = input.substring(start,pos)
  }
  function whitespace() {
    let start = pos
    while (input[pos] == ' ') ++pos
    if (input[pos] == '/' && input[pos+1] == '/') {
      pos += 2
      while (input[pos] && input[pos] != '\n') ++pos
    }
    return pos - start
  }
  //
  // parser aux
  //
  function dom(key, info, children) {
    let d = e("span",{key,info},...children)
    if (key == "error") {
      d.style["padding"] = "1px"
      d.style["background"] = "red"
    }
    return d
  }
  function collect(k,f) {
    let save = seq
    let res = []
    seq = res
    try { f() } finally {
    seq = save
    let d = dom(k,"",res)
    seq.push(d)
    }
    return res
  }
  function error(msg) {
    seq.push(dom("error", msg, []))
    collect("skip", () => {
      while (peek && peek != '\n' && peek != '*' && peek != '+' && peek != ')')
        next()
    })
    throw new Error(msg)
  }
  function expect(d) {
    if (peek == d) {
      next()
    } else {
      try { error(d+" expected") }
      catch (ex) {
        if (peek == d) next(); else throw ex
      }
    }
  }
  //
  // main parser logic
  //
  function split(d,f) {
    collect(d, () => {
      for (;;) { try { f(); } catch (ex) {}; if (peek == d) next(); else break }    })
  }
  function splitNL(d,f) {
      for (;;) { f(); if (peek == '\n' && indent == d) next(); else break }
  }
  function block(ind) {
    function expr() {
      split('=', () => {
        split('+', () => {
          split('*', () => {
            if (peek == '\n' && indent > ind) {
              collect('I'+indent, () => {
                next()
                block(indent)
                if (peek == '\n' && indent == ind) next()
              })
            } else if (peek == '(') {
              collect('()', () => {
                next()
                try { expr() } catch(ex) {} // could rethrow if no match
                expect(')')
              })
            } else if (peek == "num" || peek == "ident") {
              next()
            } else {
              error("atom expected")
            }
          })
        })
      })
    }
    splitNL(ind, () => {
      if (peek && peek != '\n')
        expr()
    })
  }
  //
  // string -> cst (concrete syntax tree: structured text as html dom)
  //
  function parse(text) {
    try {
      init(text)
      block(indent)
      let max = 20
      while (peek && max--) {
        try { error("unexpected "+peek) } catch(ex) {}
        //if (isDelimiter(peek))
          next()
        block(indent)
      }
      if (peek) error("unexpected "+peek)
      // Q: need to emit last gap?
    } catch (ex) {}
    if (pos-1 < text.length) {
      seq.push(dom("skipped","",[text.substring(pos-1,text.length)]))
    }
    return dom("P","",seq)
  }
  //
  // cst -> ast (abstract syntax tree: html dom formatted as tree)
  //
  function walk(a) {
    let ch = []
    for (let c of a.children) ch.push(walk(c))
    //if (a.key in {'+':0,'*':0,'⏎':0,'=':0,'T':0} && ch.length == 1) return ch[0]
    if (ch.length == 1) return ch[0]
    let l = (a.key + " \""+ (a.info||a.textContent) + "\"").replaceAll("\n","⏎");    let d = e("div", {}, l, ...ch)
    d.style["margin-left"] = "2ch"
    return d
  }
  //
  // main
  //
  {
    text = "3+4*5*(1+2)\n"
    let txt = parse(text)
    let ast = walk(txt)
    p.appendChild(txt)
    d.appendChild(ast)
  }