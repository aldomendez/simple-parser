let input = '2*6+4*5'

const print = (...a) => console.log(...a)
const prod = s => s.split('*').map(Number).reduce((x, y) => x * y)
const sum = s => s.split('+').map(prod).reduce((x,y)=> x + y)
print(sum(input) + ' == ' + (2 * 6 + 4 * 5))
