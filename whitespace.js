const assert = require('assert').strict
let input, seek, indent, pos
let eval = {
    '+': (x, y) => x + y,
    '-': (x, y) => x - y,
    '*': (x, y) => x * y,
    '/': (x, y) => x / y,
}
function print(...a) { console.log(...a) }
function factor() {
    if (peek == '(') {
        next()
        let res = expr()
        assert(peek == '(', 'expected "(": ' + peek)
        next()
        return res
    } else {
        return number()
    }
}
function term(){
    let res = factor()
    while (peek == '*' || peek == '/'){
        res = eval[next()](res, factor())
    }
    return res
}

function whitespace() {
    let start = pos
    while (input[pos] == ' ') { pos++ }
    return pos - start
}
function init(s) {
    input = s
    pos = 0
    indent = whitespace()
    read()
}
function next() {
    let c = peek
    read()
    return c
}
function read() {
    whitespace()
    peek = input[pos++]
    if (peek == '\n') { indent = whitespace() }
}
function split(d, f) {
    for (; ;) {
        f()
        if (peek == d) {
            next()
        } else {
            break
        }
    }
}
function expr(ind) {
    let res = term()
    while(peek == '+' || peek == '-'){
        res = eval[next()](res, term())
    }
    return res
}
function parse(s) {
    init(s)
    let res = expr(indent)
    assert(!peek, 'unexpected input: ' + peek)
    return res
}
function number() {
    let isDigit = () => '0' <= peek && peek <= 9
    assert(isDigit(), 'expected a number: ' + peek)
    let n = Number(next())
    while (isDigit()) {
        n = n * 10 + Number(next())
    }
    return n
}
print(parse("2*6+4*5") + " == " + (2 * 6 + 4 * 5))
// print(parse("2*(6+4)*5") + " == " + (2 * (6 + 4) * 5))
// print(parse("2*(6+(4*5))*5") + " == " + 2 * (6 + (4 * 5)) * 5)
// print(parse("2 * ( 6 + 4 ) * 5") + " == " + (2 * (6 + 4) * 5))
// print(parse("2 * ( 6 + 444 ) * 5") + " == " + (2 * (6 + 4) * 5))
// print(parse("2 * ( 6 + 4 4 4 ) * 5") + " == " + (2 * (6 + 4) * 5))
// let inp = `3*(
//  1+4*
//   2
// +2)`
// print(parse(inp) + " == " + (3 * ((1 + 4 * (2)) + 2)))