const assert = require('assert').strict
let input, seek, pos

function print(...a) { console.log(...a) }
function init(s) {
    input = s
    pos = 0
    peek = input[pos++]
}
function next() {
    let c = peek
    peek = input[pos++]
    return c
}
function split(d, f) {
    for (; ;) {
        f()
        if (peek == d) {
            next()
        } else {
            break
        }
    }
}
function expr() {
    let sum = 0
    split('+', () => {
        let prod = 1
        split('*', () => {
            if (peek == '(') {
                next()
                prod *= expr()
                assert(peek == ')', 'expected `)`: ' + peek)
                next()
            } else {
                prod *= number()
            }
        })
        sum += prod
    })
    return sum
}


function parse(s) {
    init(s)
    let res = expr()
    assert(!peek, 'unexpected input: ' + peek)
    return res
}
function number() {
    let isDigit = () => '0' <= peek && peek <= 9
    assert(isDigit(), 'expected a number: ' + peek)
    let n = Number(next())
    while (isDigit()) {
        n = n * 10 + Number(next())
    }
    return n
}
print(parse("2*6+4*5") + " == " + (2 * 6 + 4 * 5))
print(parse("2*(6+4)*5") + " == " + (2*(6+4)*5))
print(parse("2*(6+(4*5))*5") + " == " + 2*(6+(4*5))*5)