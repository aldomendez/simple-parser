let input = '2*6+4*5'
let sum = 0
for (let term of input.split('+')){
    let prod = 1
    for (let factor of term.split('*')){
        prod *= Number.parseInt(factor)
    }
    sum+= prod
}
print(sum + ' == ' + (2*6+4*5))

function print(...a){ console.log(...a)}